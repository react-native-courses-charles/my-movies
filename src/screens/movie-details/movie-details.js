import React from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import MovieSection from "../../shared/components/movie-section/movie-section";
import { WebView } from 'react-native-webview';

class MovieDetailsScreen extends React.Component {
    state = {
        movie: {
            name: 'The Hurt Locker',
            year: 2008,
            image: 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/3ZEqU9Ykmn8zGDUwWnmTfHaaWRB.jpg',
            overview: 'Forced to play a dangerous game of cat-and-mouse in the chaos of war, an elite Army bomb squad unit must come together in a city where everyone is a potential enemy and every object could be a deadly bomb.'
        },
        realated: {
            name: 'Related',
            hideMoreButton: true,
            movies: [
                {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/7BsvSuDQuoqhWmU2fL7W2GOcZHU.jpg'
                }, {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/k4FwHlMhuRR5BISY2Gm2QZHlH5Q.jpg'
                }, {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/qAwFbszz0kRyTuXmMeKQZCX3Q2O.jpg'
                }, {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/ngKxbvsn9Si5TYVJfi1EGAGwThU.jpg'
                },
            ]
        },
        loadingTrailer: true
    }

    render() {
        return (
            <ScrollView>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Image
                        style={{ width: 102, height: 150, margin: 10 }}
                        source={{ uri: this.state.movie.image }}
                    />
                </View>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 20}}>{this.state.movie.name} ({this.state.movie.year})</Text>
                </View>
                <View style={{flex: 1, padding: 15, paddingTop: 15}}>
                    <Text style={{fontSize: 15, textAlign: 'justify'}}>{this.state.movie.overview}</Text>
                </View>
                <View style={{flex: 1, padding: 15, paddingTop: 15}}>
                        {this.state.loadingTrailer ? <View style={{
                            width: '100%',
                            height: 300,
                            opacity: .5,
                            backgroundColor: 'black',
                            position: 'absolute',
                            zIndex: 1000,
                            marginLeft: 15
                        }}>
                            <Text style={{color: 'white', fontSize: 20}}>Loading...</Text>
                        </View> : null}
                        <WebView 
                        style={{height: 300, alignSelf: 'stretch'}}
                        source={{ uri: 'https://www.youtube.com/embed/RJa4kG1N3d0' }}
                        onLoadEnd={() => {
                            this.setState({loadingTrailer: false});
                        }}
                         />
                </View>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                    paddingLeft: 15,
                    paddingRight: 15,
                    marginTop: 20,
                    marginBottom: 5
                }}>
                    <TouchableOpacity
                        style={{
                            backgroundColor: 'red',
                            padding: 20,
                            paddingTop: 10,
                            paddingBottom: 10,
                            borderRadius: 5
                        }}
                        onPress={() => {

                        }}>
                        <Text style={{color: 'white', fontWeight: 'bold'}}>Watch Trailer</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{
                            backgroundColor: 'red',
                            padding: 20,
                            paddingTop: 10,
                            paddingBottom: 10,
                            borderRadius: 5
                        }}
                        onPress={() => {

                        }}>
                        <Text style={{color: 'white', fontWeight: 'bold'}}>+ Favorite</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex: 1, padding: 10}}>
                    <MovieSection navigation={this.props.navigation} section={this.state.realated} styles={{padding: 10}} />
                </View>
            </ScrollView>
        );
    }
}

export default MovieDetailsScreen;