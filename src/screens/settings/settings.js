import React from "react";
import { FlatList, View, Text, ScrollView, TouchableOpacity } from "react-native";

export default class SettingsScreen extends React.Component {
    static navigationOptions = {
        title: 'Settings',
    };
    state = {
        menuItens: [
            {title: 'Rate'},
            {title: 'Sign Out'}
        ]
    }
    render() {
        return (
            <ScrollView style={{flex: 1}}>
                <FlatList
                style={{flex: 1}}
                data={this.state.menuItens}
                keyExtractor={(item) => item.title}
                renderItem={({item}) => {
                    return (
                        <View style={{flex: 1}}>
                            <TouchableOpacity
                                onPress={() => {}}>
                                <Text>{item.title}</Text>
                            </TouchableOpacity>
                        </View>
                    );
                }}
                />
            </ScrollView>
        );
    }
}