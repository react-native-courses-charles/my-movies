import React from "react";
import { View, Text, Image,FlatList, ScrollView, TextInput } from "react-native";
import { moviesService } from "../../services/movies";
import MovieSection from "../../shared/components/movie-section/movie-section";

class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Browse',
    };

    state = {
        moviesSection: moviesService.moviesSection
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <View style={{padding: 10}}>
                    <TextInput
                        style={{
                            borderWidth: 1,
                            borderColor: '#ccc',
                            padding: 10,
                            borderRadius: 5 
                        }}
                        placeholder='Search...'></TextInput>
                </View>
                <FlatList style={{ flex: 1 }}
                    data={this.state.moviesSection}
                    keyExtractor={(item) => item.name}
                    renderItem={({ item }) => {
                        return (
                            <MovieSection onItemClicked={(item) => {
                                this.props.navigation.navigate('Details', {id: 10});
                            }} section={item} loading={item.loading}/>
                        );
                    }}
                />
            </View>
        );
    }
}

export default HomeScreen;