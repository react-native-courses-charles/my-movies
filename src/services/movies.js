export const moviesService = {
    moviesSection: [
        {
            name: 'Top Rated',
            movies: [
                {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/7BsvSuDQuoqhWmU2fL7W2GOcZHU.jpg'
                }, {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/k4FwHlMhuRR5BISY2Gm2QZHlH5Q.jpg'
                }, {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/qAwFbszz0kRyTuXmMeKQZCX3Q2O.jpg'
                }, {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/ngKxbvsn9Si5TYVJfi1EGAGwThU.jpg'
                },
            ],loading: false
        },
        {
            name: 'Comedies',
            movies: [
                {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/7BsvSuDQuoqhWmU2fL7W2GOcZHU.jpg'
                },
            ],loading: true
        }, {
            name: 'Last Releases',
            movies: [
                {
                    image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/7BsvSuDQuoqhWmU2fL7W2GOcZHU.jpg'
                },
            ],loading: true
        }
    ]
}