import React from "react";
import { View, Text, Image, Dimensions, FlatList, TouchableOpacity } from "react-native";
import ContentLoader from 'rn-content-loader';
import { Circle, Rect } from "react-native-svg";

class MovieSection extends React.Component {
    render() {
        return (
            <View style={[{ marginBottom: 25, paddingLeft: 10, paddingRight: 10 }, this.props.styles]}>
                {/* Section Title */}
                <View style={[{
                    marginBottom: 10,
                    flex: 1,
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    alignItems: 'flex-end'
                }]}>
                    <Text style={{ fontSize: 20 }}>{this.props.section.name}</Text>
                    {/* {this.props.hideMoreButton ? null : <Text style={{ fontSize: 15, marginTop: 5 }}>More...</Text>} */}
                </View>

                {/* Section body */}
                <View>
                    {this.props.loading ? 
                    <ContentLoader height={150} width={Dimensions.get('window').width - 20}>
                        <Rect x="0" y="0" rx="5" ry="5" width="102" height="150" />
                        <Rect x="110" y="0" rx="5" ry="5" width="102" height="150" />
                        <Rect x="220" y="0" rx="5" ry="5" width="102" height="150" />
                        <Rect x="330" y="0" rx="5" ry="5" width="102" height="150" />
                    </ContentLoader> : 
                    <FlatList style={{ flex: 1 }}
                        data={this.props.section.movies}
                        horizontal={true}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        if (this.props.onItemClicked) {
                                            this.props.onItemClicked();
                                        }
                                    }}>
                                    <Image
                                            style={{ width: 102, height: 150, marginRight: 8 }}
                                            source={{ uri: item.image }}
                                        />
                                </TouchableOpacity>
                            );
                        }}
                    />}
                </View>
            </View>
        );
    }
}

export default MovieSection;